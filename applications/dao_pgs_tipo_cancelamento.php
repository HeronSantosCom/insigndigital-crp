<?php

class dao_pgs_tipo_cancelamento {

    public static function pegar($codigo) {
        $db = new mysqlsearch();
        $db->table("pgs_tipo_cancelamento");
        $db->column("*");
        $db->match("codigo", $codigo);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    private static function hook($row) {
        return $row;
    }

}
<?php

if (!class_exists("pagseguro", false)) {
    include path::plugins("pagseguro.php");
}

class main extends app {

    public function __construct() {
        $subdominio = substr(domain, 0, strpos(domain, "."));
        switch ($subdominio) {
            case "pagseguro":
                $this->pagseguro();
                break;
        }
    }

    public function teste() {
        $x = dao_pgs_tipo_status::listar();
        knife::dump($x);
    }

    private function pagseguro() {
        if (!empty($_POST["notificationType"]) && $_POST["notificationType"] == "transaction") {
            $retorno = pagseguro::notificacao($_POST["notificationCode"]);
            if ($retorno) {
                $cobranca = dao_cobranca::pegar($retorno->reference);
                $pgs_tipo_cancelamento = (!empty($retorno->cancellationSource) ? dao_pgs_tipo_cancelamento::pegar($retorno->cancellationSource) : false);
                $pgs_tipo_status = dao_pgs_tipo_status::pegar($retorno->status);
                if (dao_pgs_transacao::cadastrar($retorno->code, $retorno->installmentCount, ($pgs_tipo_cancelamento ? $pgs_tipo_cancelamento["id"] : false), $retorno->status, $retorno->type, $retorno->paymentMethod->code, $cobranca["id"])) {
                    // se a cobranca já tiver sido paga, ou está ok não fazer nada, senão atualizar cobranca
                    switch ($cobranca["tipo_pagamento_status_id"]) {
                        case 3:
                        case 4:
                            break;
                        default:
                            dao_cobranca::atualizar($cobranca["id"], $retorno->code, 2, ($pgs_tipo_status ? $pgs_tipo_status["tipo_pagamento_status_id"] : false));
                            break;
                    }
                }
            }
        }
    }

}
<?php

class dao_pgs_tipo_status {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("pgs_tipo_status");
        $db->column("*");
        $db->match("id", $id);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $db = new mysqlsearch();
        $db->table("pgs_tipo_status");
        $db->column("*");
        return $db->go();
    }

    private static function hook($row) {
        return $row;
    }

}
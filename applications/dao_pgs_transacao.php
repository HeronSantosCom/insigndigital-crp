<?php

class dao_pgs_transacao {

    public static function cadastrar($codigo, $parcelas, $pgs_tipo_cancelamento_id, $pgs_tipo_status_id, $pgs_tipo_transacao_id, $pgs_tipo_meio_pagamento_id, $cobranca_id) {
        $db = new mysqlsave();
        $db->table("pgs_transacao");
        $db->column("codigo", $codigo);
        $db->column("parcelas", $parcelas);
        $db->column("cadastrado", date("Y-m-d H:i:s"));
        $db->column("pgs_tipo_cancelamento_id", $pgs_tipo_cancelamento_id);
        $db->column("pgs_tipo_status_id", $pgs_tipo_status_id);
        $db->column("pgs_tipo_transacao_id", $pgs_tipo_transacao_id);
        $db->column("pgs_tipo_meio_pagamento_id", $pgs_tipo_meio_pagamento_id);
        $db->column("cobranca_id", $cobranca_id);
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

}
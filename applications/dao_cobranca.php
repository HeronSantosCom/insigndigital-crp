<?php

class dao_cobranca {

    public static function pegar($codigo) {
        $db = new mysqlsearch();
        $db->table("cobranca");
        $db->column("*");
        $db->match("codigo", $codigo);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    private static function hook($row) {
        return $row;
    }

    public static function atualizar($id, $referencia, $tipo_pagamento_id, $tipo_pagamento_status_id) {
        $db = new mysqlsave();
        $db->table("cobranca");
        $db->column("referencia", $referencia);
        $db->column("tipo_pagamento_id", $tipo_pagamento_id);
        $db->column("tipo_pagamento_status_id", $tipo_pagamento_status_id);
        $db->match("id", $id);
        return $db->go();
    }

}